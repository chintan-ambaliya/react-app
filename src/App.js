import './App.scss';

import {useEffect} from "react";
import {useDispatch} from 'react-redux';
import {useHistory} from "react-router-dom";

import Routing from './routing/routing';
import DefaultLayout from "./components/layouts/default_layout/DefaultLayout";
import {authActions} from "./store/auth-store";
import utils from "./utils";

function App() {
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        let token = utils.get_cookie('token');
        if (token) {
            dispatch(authActions.login({token}));
            history.push('/dashboard');
        } else {
            history.push('/login');
        }
    }, [dispatch, history]);

    return (
        <DefaultLayout>
            <Routing/>
        </DefaultLayout>
    );
}

export default App;
