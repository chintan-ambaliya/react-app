import {Link} from "react-router-dom";
import {useContext} from "react";

import authContext from "../../store/auth-store";

const Navbar = (prop) => {
    const context = useContext(authContext);

    return (
        <nav className="navbar navbar-expand-sm bg-danger navbar-dark justify-content-end">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to='/dashboard'>Dashboard</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/Profile">Profile</Link>
                </li>
                {
                    context.isLoggedIn &&
                    <li className="nav-item">
                        <span className="nav-link" onClick={context.logout}>Logout</span>
                    </li>
                }
            </ul>
        </nav>
    );
}

export default Navbar;
