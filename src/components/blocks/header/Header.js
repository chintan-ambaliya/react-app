import './Header.scss';

import {NavLink, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux';
import Menus from "./../../../routing/links";
import {useEffect, useState} from "react";
import rpc from "../../../api/rpc";
import {authActions} from "../../../store/auth-store";

const Header = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [menuLinks, setMenuLinks] = useState([])
    const isLoggedIn = useSelector(state => state.auth.isLoggedIn);

    useEffect(() => {
        setMenuLinks(() => {
            return Menus.Links.filter((link) => {
                if ((link.name === 'Login' && isLoggedIn)
                    || (link.name === 'Signup' && isLoggedIn)
                    || (link.name === 'Dashboard' && !isLoggedIn)) {
                    return false;
                }
                return true;
            });
        });
    }, [isLoggedIn]);

    const logoutHandler = async (ev) => {
        ev.stopPropagation();
        const logout = async () => {
            await rpc.query({
                route: '/api/json/auth/delete/token',
            });
        }
        await logout();
        history.push('/login');
        dispatch(authActions.logout());
    }

    return (
        <header>
            <nav className="navbar navbar-expand-sm bg-danger navbar-dark justify-content-end">
                <ul className="navbar-nav">
                    {menuLinks.map(link => <NavLink className="nav-link" key={link.name} to={link.path}>{link.name}</NavLink>)}
                    {isLoggedIn && <a href={'#!'} className="nav-link" onClick={logoutHandler}>Logout</a>}
                </ul>
            </nav>
        </header>
    );
};

export default Header;
