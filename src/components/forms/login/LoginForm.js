import {useState} from "react";
import {useHistory} from "react-router-dom";
import {useSelector, useDispatch} from 'react-redux';

import {authActions} from "../../../store/auth-store";
import {toastActions} from "../../../store/toast-store";
import rpc from "../../../api/rpc";

const LoginForm = (props) => {
    const dispatch = useDispatch();
    const isLoginError = useSelector(state => state.auth.isLoginError);
    const loginErrorMessage = useSelector(state => state.auth.loginErrorMessage);

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isValidFormData, setIsValidFormData] = useState(null);

    const emailHandler = (event) => {
        setEmail(event.target.value);
    };

    const passwordHandler = (event) => {
        setPassword(event.target.value);
    };

    const login = async (email, password) => {
        const response = await rpc.query({
            route: '/api/json/auth/token',
            params: {
                "db": "react_v14",
                "username": email,
                "password": password,
            }
        });
        return response.result;
    }

    const formSubmitHandler = (event) => {
        event.preventDefault();
        if (email && password) {
            setIsValidFormData(true);
            dispatch(toastActions.hide());
            login(email, password).then((result) => {
                if (result && result.access_token) {
                    dispatch(authActions.login({token: result.access_token}));
                    history.push('/dashboard');
                }
                if (result.error) {
                    dispatch(toastActions.show({
                        title: 'Error',
                        message: result.error.message,
                        type: 'danger',
                        sticky: true,
                    }));
                }
            });
        } else {
            setIsValidFormData(false);
        }
    };

    return (
        <div className="row mt-5">
            <div className="col-md-4 offset-md-4">
                <form>
                    {
                        isValidFormData === false &&
                        <div className="alert alert-danger">
                            Invalid input
                        </div>
                    }
                    {
                        isLoginError &&
                        <div className="alert alert-danger">
                            {loginErrorMessage}
                        </div>
                    }
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" value={email} onChange={emailHandler}
                               placeholder="Enter email" id="email"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" value={password} onChange={passwordHandler}
                               placeholder="Enter password" id="pwd"/>
                    </div>
                    <button type="submit" className="btn w-100 btn-primary mt-2" onClick={formSubmitHandler}>Login</button>
                </form>
            </div>
        </div>
    );
}

export default LoginForm;
