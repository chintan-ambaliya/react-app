import './DefaultLayout.scss';

import {Fragment} from "react";
import {useSelector} from "react-redux";

import Header from "../../blocks/header/Header";
import Footer from "../../blocks/footer/Footer";
import Toast from "../../toast/Toast";

const DefaultLayout = (props) => {
    const toastTitle = useSelector(state => state.toast.title);
    const toastMessage = useSelector(state => state.toast.message);
    const toastType = useSelector(state => state.toast.type);
    const toastIsShow = useSelector(state => state.toast.isShow);
    const toastSticky = useSelector(state => state.toast.sticky);
    return (
        <Fragment>
            {toastIsShow && <Toast title={toastTitle} message={toastMessage} type={toastType} sticky={toastSticky}/>}
            <div className={'wrap'}>
                <Header/>
                <main className={'container'}>
                    {props.children}
                </main>
                <Footer/>
            </div>
        </Fragment>
    );
};

export default DefaultLayout;
