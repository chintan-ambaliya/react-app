import './Toast.scss';

import {useEffect, useRef} from "react";
import {useDispatch} from "react-redux";
import {toastActions} from "../../store/toast-store";

const BGType = {
    primary: 'bg-primary',
    info: 'bg-info',
    success: 'bg-success',
    warning: 'bg-warning',
    danger: 'bg-danger',
}

const Toast = (props) => {
    const dispatch = useDispatch();
    const toastEl = useRef(null);

    const closeHandler = () => {
        toastEl.current.classList.remove('show');
        setTimeout(() => dispatch(toastActions.hide()), 1000);
    }

    useEffect(() => {
        if (toastEl.current){
            toastEl.current.classList.add('show');
        }
    }, [toastEl]);

    if (!props.sticky) {
        setTimeout(() => {
            closeHandler();
        }, 5000);
    }

    return (
        <div className={'o_toast'}>
            <div className={`toast ${BGType[props.type] || ''}`} ref={toastEl}>
                <div className="toast-header">
                    <strong className="mr-auto">{props.title}</strong>
                    <small></small>
                    <button type="button" className="ml-2 mb-1 close" onClick={closeHandler}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="toast-body">
                    {props.message}
                </div>
            </div>
        </div>
    )
}

export default Toast;
