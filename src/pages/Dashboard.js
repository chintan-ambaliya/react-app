import {useCallback, useEffect} from "react";
import {useSelector, useDispatch} from 'react-redux';
import {authActions} from "../store/auth-store";

import rpc from "../api/rpc";

const Dashboard = () => {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(state => state.auth.isLoggedIn)
    const resPartnerData = useSelector(state => state.auth.resPartnerData)

    const getResPartnerDataHandler = useCallback(async () => {
        const response = await rpc.query({
            "model": "res.partner",
            "method": "search_read",
            "kwargs": {
                "domain": [],
                "fields": ["name", "email", "phone"],
                "limit": 80,
            },
        });

        dispatch(authActions.setResPartnerData(response.result));
        if (response.result.error) {
            alert(response.result.error.message);
        }
    }, [dispatch]);

    useEffect(() => {
        if (isLoggedIn) {
            getResPartnerDataHandler();
        }
    }, [isLoggedIn, getResPartnerDataHandler]);

    return (
        <div className="row mt-5">
            <div className="col-md-12">
                <h1 className="text-center">
                    Partner Data
                </h1>
                <div className="table-responsive">
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            isLoggedIn && resPartnerData.map((record) => {
                                return (<tr key={record.id}>
                                    <td>{record.name}</td>
                                    <td>{record.email}</td>
                                    <td>{record.phone}</td>
                                </tr>);
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Dashboard;
