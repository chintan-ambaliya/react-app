import {Fragment} from "react";
import LoginForm from "../components/forms/login/LoginForm";

const Login = () => {
    return (
        <Fragment>
            <LoginForm/>
        </Fragment>
    );
}

export default Login;
