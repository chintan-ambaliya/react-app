import Login from "./Login";
import Signup from "./Signup";
import Dashboard from "./Dashboard";

const Pages = {
    LoginPage: Login,
    SignupPage: Signup,
    DashboardPage: Dashboard,
};

export default Pages;
