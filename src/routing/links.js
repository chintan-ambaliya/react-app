import Pages from "../pages";

const Routes = [
    {
        name: 'Login',
        path: '/login',
        component: Pages.LoginPage,
    },
    {
        name: 'Signup',
        path: '/signup',
        component: Pages.SignupPage,
    },
    {
        name: 'Dashboard',
        path: '/dashboard',
        component: Pages.DashboardPage,
    },
];

const Links = [
    {
        name: 'Login',
        path: '/login',
    },
    {
        name: 'Signup',
        path: '/signup',
    },
    {
        name: 'Dashboard',
        path: '/dashboard',
    },
];

const data = {Routes, Links};

export default data;
