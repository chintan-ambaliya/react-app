import {Switch, Route} from "react-router-dom";
import Menus from "./links";

const Routing = () => {
    return (
        <Switch>
            {Menus.Routes.map(route => <Route key={route.name} exact {...route}/>)}
        </Switch>
    );
};

export default Routing;
