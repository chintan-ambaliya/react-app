import {createSlice} from "@reduxjs/toolkit";

import utils from "../utils";

const initialState = {
    isLoggedIn: false,
    isLoading: false,
    isLoginError: false,
    loginErrorMessage: '',
    token: '',
    resPartnerData: [],
}

const authSlice = createSlice({
    name: 'Auth',
    initialState: initialState,
    reducers: {
        login(state, action) {
            state.isLoggedIn = true;
            state.isLoginError = false;
            state.loginErrorMessage = '';
            state.token = action.payload.token;
            utils.set_cookie('token', action.payload.token);
        },
        logout(state) {
            state = initialState;
            utils.set_cookie('token', '', 0);
        },
        setResPartnerData(state, action) {
            state.resPartnerData = action.payload;
        },
    }
});

export default authSlice;
export const authActions = authSlice.actions;
