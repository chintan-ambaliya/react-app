import {configureStore} from "@reduxjs/toolkit";

import AuthSlice from './auth-store';
import ToastSlice from './toast-store';

const store = configureStore({
    reducer: {
        auth: AuthSlice.reducer,
        toast: ToastSlice.reducer
    },
})

export default store;
