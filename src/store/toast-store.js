import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    title: false,
    message: false,
    type: false,
    isShow: false,
    sticky: false,
}

const toastSlice = createSlice({
    name: 'Toast',
    initialState: initialState,
    reducers: {
        show(state, action) {
            state.title = action.payload.title;
            state.message = action.payload.message;
            state.type = action.payload.type;
            state.isShow = true;
            state.sticky = action.payload.sticky;
        },
        hide(state) {
            state.title = false;
            state.message = false;
            state.type = false;
            state.isShow = false;
            state.sticky = false;
        }
    }
});

export default toastSlice;
export const toastActions = toastSlice.actions;
