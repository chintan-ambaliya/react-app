const utils = {
    /**
     * Read the cookie described by c_name
     *
     * @param {string} c_name
     * @returns {string}
     */
    get_cookie: function (c_name) {
        var cookies = document.cookie ? document.cookie.split('; ') : [];
        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = parts.shift();
            var cookie = parts.join('=');

            if (c_name && c_name === name) {
                return cookie;
            }
        }
        return "";
    },
    /**
     * Create a cookie
     * @param {String} name the name of the cookie
     * @param {String} value the value stored in the cookie
     * @param {Integer} ttl time to live of the cookie in millis. -1 to erase the cookie.
     */
    set_cookie: function (name, value, ttl) {
        ttl = ttl || 24 * 60 * 60 * 365;
        document.cookie = [
            name + '=' + value,
            'path=/',
            'max-age=' + ttl,
            'expires=' + new Date(new Date().getTime() + ttl * 1000).toGMTString()
        ].join(';');
    },
};

export default utils;